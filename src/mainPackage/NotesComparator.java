/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mainPackage;

import java.util.Comparator;

/**
 *
 * @author Kamilius
 */
public class NotesComparator implements Comparator<Note> {
    @Override
    public int compare(Note o1, Note o2) {
        if (o1.Id < o2.Id)
            return 1;
        else if (o1.Id > o2.Id)
            return -1;
        else
            return 0;
    }
}
