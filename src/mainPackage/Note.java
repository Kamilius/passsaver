/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mainPackage;

/**
 *
 * @author Kamilius
 */
public class Note {
    public int Id = 0;
    public String WebSite = "";
    public String User = "";
    public String Password = "";
    public String Description = "";
    
    public Note(int id, String site, String user, String pass, String description) {
        Id = id;
        WebSite = site;
        User = user;
        Password = pass;
        Description = description;
    }
}
