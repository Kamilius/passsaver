/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mainPackage;

import java.sql.*;
import java.util.ArrayList;

/**
 *
 * @author kamilius
 */
public class SQLConnector {
    // JDBC driver name and database URL
    static String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static String DB_URL = "jdbc:mysql://localhost/PassSaver";
    
    //  Database credentials
    static final String USER = "root";
    static final String PASS = "1111";
    
    public Connection conn = null;
    public Statement stmt = null;

    public boolean OpenConnection() {
        boolean connected = false;
        try {
            Class.forName(JDBC_DRIVER);
            
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
            
            connected = true;
        } catch(Exception e){
           //Handle errors for Class.forName
           e.printStackTrace();
        }
        return connected;
    }
    public boolean CloseConnection() {
        boolean disconnected = false;
        try {
            stmt.close();
            conn.close();
            
            disconnected = true;
        } catch(Exception e) {
           e.printStackTrace();
        } finally {
            try{
               if(stmt!=null)
                  stmt.close();
            }catch(SQLException se2){
            }
            try{
               if(conn!=null)
                  conn.close();
            }catch(SQLException se){
               se.printStackTrace();
            }
        }
        return disconnected;
    }
    public ArrayList<Note> GetNotes() {
        OpenConnection();
        ResultSet rs = null;
        ArrayList<Note> notes = new ArrayList<>();
        
        try {
            rs = stmt.executeQuery("SELECT * FROM Notes;");
            while(rs.next()) {
                notes.add(new Note(rs.getInt("Id"),
                                   rs.getString("WebSite"), 
                                   rs.getString("User"),
                                   rs.getString("Password"), 
                                   rs.getString("Description")));
            }            
        } catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
        }
        CloseConnection();
        return notes;
    }
    public boolean SaveNote(Note note) {
        boolean saved = false;
        String insertQuery = "INSERT INTO Notes(WebSite,User,Password,Description)"
                + " VALUES('" + note.WebSite + "','" + note.User + "','"
                + note.Password + "','" + note.Description + "');";
                
        OpenConnection();
        
        try {
            stmt.executeUpdate(insertQuery);
            saved = true;
        } catch (SQLException e) {
            
        }
        
        CloseConnection();
        return saved;
    }
    public Note GetNoteById(int id) {
        ResultSet rs = null;
        Note note = null;
        String selectQuery = "SELECT * FROM Notes WHERE Id=" + id + ";";
        
        
        OpenConnection();
        
        try {
            rs = stmt.executeQuery(selectQuery);
            rs.first();
            note = new Note(rs.getInt("Id"),
                            rs.getString("WebSite"), 
                            rs.getString("User"),
                            rs.getString("Password"), 
                            rs.getString("Description"));
        } catch (SQLException e) {
            
        }
        
        CloseConnection();
        return note;
    }
    public boolean EditNote(Note note) {
        boolean result = false;
        String updateQuery = "UPDATE Notes SET WebSite='"+ note.WebSite +"', User='"+ note.User +"',"
                + " Password='"+note.Password+"', Description='"+note.Description+"' WHERE Id="+ note.Id +";";
        
        OpenConnection();
        
        try {
            int queryRes = stmt.executeUpdate(updateQuery);
            if(queryRes == 1)
                result = true;
        } catch (SQLException e) {
            
        }
        
        CloseConnection();
        return result;
    }
    public boolean RemoveNote(int Id) {
        boolean result = false;
        String updateQuery = "DELETE FROM Notes WHERE Id="+ Id +";";
        
        OpenConnection();
        
        try {
            stmt.executeUpdate(updateQuery);
            result = true;
        } catch (SQLException e) {
            
        }
        
        CloseConnection();
        return result;
    }
}
