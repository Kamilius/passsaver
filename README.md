### How do I get set up? ###

* Open MySQL server through terminal: myqsl -u root -p
* Set root@localhost password: set password for root@localhost = password('1111'); flush privileges; quit;
* Create database and table:
CREATE DATABASE PassSaver;
USE PassSaver;
CREATE TABLE Notes (Id INT NOT NULL AUTO_INCREMENT, WebSite VARCHAR(250), User VARCHAR(250), Password VARCHAR(250), Description VARCHAR(250), PRIMARY KEY (Id));